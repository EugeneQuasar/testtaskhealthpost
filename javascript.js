let burger = document.querySelector(".header__burger_wrap");
let burgerItem = document.querySelector(".header__burger_item");
let headerNav = document.querySelector(".header__nav-bg");

document.addEventListener("click", (event) => {
  let targetElement = event.target;
  if ((targetElement == burger) || (targetElement == burgerItem)) {
    headerNav.classList.add("header__nav_show");
  } else if (true) {
    do {
      if (targetElement == headerNav) {
        return;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    headerNav.classList.remove("header__nav_show");
  }
});
